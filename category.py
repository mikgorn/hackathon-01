from collections import defaultdict

class Category():
        def __init__(self,name=""):
                self.name = name
                self.parent = None
                self.parent_name =""
                self.children = []
                self.words=defaultdict(int)
                self.products={}

def add_child(parent,child):
        try:
                parent.children.append(child)
                child.parent = parent
                parent.words.update(child.words)
        except:
                print("ERROR")
                print(parent.name, parent.parent_name)
                print(parent.name, parent.parent_name)

def init_root():
        root = Category()
        goods = Category("Товары")
        service = Category("Услуги")
        work = Category("Работы")
        add_child(root,goods)
        add_child(root,service)
        add_child(root,work)
        return root

def print_category(category,n):
        print(' '*n,category.name)
        for  child in category.children:
                print_category(child,n+1)   

def find_category(list,name):
        for cat in list:
                if(cat.name==name):
                        return cat
        return None