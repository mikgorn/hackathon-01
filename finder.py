import base
from collections import defaultdict
from category import *
from crutch_tree import Tree

def process_line(line):
        line = line.lower()
        line = line.translate({ord(c): " " for c in "!@#$%^&*()[]\{\}\"\';:,./<>?\|`~-=_+«»"})
        return line

class Finder(base.Finder):
        words ={}
        words_counter={}
        
        def __init__(self, db: base.Database, simplifier: base.Simplifier):
                self.categories=[]
                self.all_words =defaultdict(int)
                rows = db.get_values_using_filter(lambda a : True)
                i=0
                nrows = len(rows)
                for row in rows:
                        print("Adding items "+str(i)+"/"+str(nrows))
                        if i==0:
                                i+=1
                                continue
                        i+=1
                        fullname=[]
                        name = ""
                        try:
                                fullname = simplifier.simplify(row[1])
                                name = ' '.join(fullname)
                        except:
                                print("error")
                        cat1 = row[4]
                        cat0 = row[5]
                        cat = find_category(self.categories,cat0)
                        if cat==None:
                                cat = Category()
                                cat.name = cat0
                                cat.parent_name = cat1
                                self.categories.append(cat)
                                print("Добавлена категория \""+cat0+"\" в  \""+cat1+'\"')
                        for word in fullname:
                                if not (word in self.all_words):
                                        self.all_words[word]+=1
                                        cat.words[word]+=1
                                        cat.products.update({name:(i-1)})
                i = 0
                for pair in (self.all_words.items()):
                        self.words.update({pair[0]:i})
                        self.words_counter.update({i:pair[1]})
                        i+=1
                self.category_tree = Tree(self.words,self.words_counter)
                i=0
                nrows = len(self.categories)
                for category in self.categories:
                        print("Building trees "+str(i)+"/"+str(nrows))
                        if category.words is None:
                                print("error at "+category.name)
                        i+=1
                        fullname =[]
                        for pair in (category.products.items()):
                                self.category_tree.set_value(pair[0].split(),category.name)
                


        def get_values(self,key_list: str) :
                try:
                        result = []

                        category_answer = self.category_tree.get_values(key_list)
                        print(category_answer)
                        target_categories=[]
                        for answer in category_answer:
                                if (answer.value!=[]):
                                        new_cat = find_category(self.categories,answer.value[0])
                                        if not (new_cat in target_categories):
                                                target_categories.append(new_cat)
                        
                        print(target_categories)
                        for category in target_categories:
                                new_words={}
                                new_words_counter = {}
                                i = 0
                                for pair in (category.words.items()):
                                        new_words.update({pair[0]:i})
                                        new_words_counter.update({i:pair[1]})
                                        i+=1
                                new_category_tree = Tree(new_words,new_words_counter)

                                for key,value in category.products.items():
                                        fullname = key.split()
                                        print("no error")
                                        print(fullname)
                                        print(value)
                                        new_category_tree.set_value(fullname,value)
                                
                                result_tmp= new_category_tree.get_values(key_list)
                                
                                for answer in result_tmp:
                                        if (answer.value!=[]):
                                                result.append(answer)                     
                        return result
                except Exception as exc:
                        print(exc)
                        print(self.words)
                        print(self.words_counter)
                        return []

                


