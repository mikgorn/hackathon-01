import copy
from typing import *
from json import loads

from flask import Flask, jsonify, request
from flask_ngrok import run_with_ngrok

import base
import core
import database
import simplifier

app = Flask(__name__)
run_with_ngrok(app)

#
from collections import defaultdict
import crutch_tree


JSON_TMP = {
            "Производитель": "",
            "Страна_происхождения": "",
            "Id": "",
            "Наименование": "",
            "Вид_товаров": "",
            "Вид_продукции": "",
            "Длина": "",
            "Высота": "",
            "Ширина": "",
            "Материал": "",
            "Диаметр": "",
            "Гарантийный_срок": "",
            "Цвет": "",
            "Вес": "",
            "Объем": "",
            "Количество_действующих_оферт": "",
            "Сумма_в_составе_контрактов": None,
            "Количество_контрактов": None,
        }

    #return jsonify([{"correct_text": "Инкоррект", "Id": "cfff"}])


class Frontender:
    def __init__(self, core: core.Core):
        self._core = core

    @app.route("/find/<key>", methods=["POST", "GET"])
    def hell(self, key: str):
        text1, text2 = self._core.check_errors(key)
        if text1 != text2:
            return jsonify([{"correct_text": text2, "Id": text1}])
        results = self._core.get_answer(key)
        print(results)
        return jsonify(
        )



class EasyFinder(base.Finder):
    def __init__(self, db: base.Database, simplifier: base.Simplifier):
        self.db = db
        self.simplifier = simplifier
        id = 1
        id_ = 1
        words = {}
        words_counter = defaultdict(int)
        normalize_text = []
        for text in self.db.get_vertical_vector(1, self.db.length-80000, 1):
            print(text)
            items = self.simplifier.simplify(str(text))
            normalize_text.append((items, id_))
            id_ += 1
            for item in items:
                if item not in words:
                    words[item] = id
                    id += 1
                words_counter[item] += 1
        self.words, self.words_counter = words, words_counter
        self.tree = crutch_tree.Tree(words, words_counter)
        #print(words)
        #print(words_counter)
        #print(normalize_text)
        from time import perf_counter
        t_st = perf_counter()
        print(len(normalize_text))
        c = 0
        for k in normalize_text:
            c += 1
            print(k[0])
            print(c)
            self.tree.set_value(*k)
        print(perf_counter() - t_st)

    def get_values(self, key: List[str]) -> List[Any]:
        return self.tree.get_values(key)


@app.after_request
def after_request(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
    return response


if __name__ == '__main__':
    import finder
    db = database.connect_to_db("./Dataset.xlsx")
    #ef = EasyFinder(db, simplifier.Simplifier())
    ff = finder.Finder(db, simplifier.Simplifier())
    c = core.Core(db, simplifier.Simplifier(), ff)
    while True:
        request = input("Enter your request: \n")
        print(c.get_answer(request))
    print(c.get_answer("посуда"))
    #app.run()