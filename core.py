import os
import logging
from typing import *
from time import sleep, perf_counter
from queue import Queue as ThreadQueue
from threading import Thread
from collections import defaultdict

import base
import simplifier
from database import connect_to_db


TIMEOUT = 20


class Core(base.Core):
    _NAMES_POSITION = 1

    class _Preparer:
        def __init__(self, q_in: ThreadQueue, q_out: ThreadQueue, simplifier: base.Simplifier):
            super().__init__()
            self.q_in = q_in
            self.q_out = q_out
            self.simplifier = simplifier

        def prepare(self) -> None:
            while True:
                text = self.q_in.get()
                if text is None:
                    return
                self.q_out.put(self.simplifier.simplify(text))

    class _Counter:
        def __init__(self, q_in: ThreadQueue):
            super().__init__()
            self._id = 0
            self.q_in: ThreadQueue = q_in
            self.words = {}
            self.words_counter = defaultdict(int)

        def prepare(self) -> None:
            while True:
                items = self.q_in.get()
                if items is None:
                    return
                for word in items:
                    if word not in self.words:
                        self.words[word] = self._id
                        self._id += 1
                    self.words_counter[word] += 1

    def __init__(self, db: base.Database, simplifier: base.Simplifier, finder: base.Finder, one_thread_prepare: bool = False):
        self.db = db
        self.simplifier = simplifier
        self.finder = finder
        self.words, self.words_counter = self.finder.words, self.finder.words_counter
        self._one_thread_prepare = one_thread_prepare

    def get_answer(self, key: str) -> List[Any]:
        simple_key = self.simplifier.simplify(key)
        answer = self.finder.get_values([k for k in simple_key if k in self.words])
        result = []
        for r in answer:
            result.extend(r.value)
        full_result = []
        for r in result:
            full_result.append(self.db[r])
        return full_result

    def check_errors(self, key: str) -> Tuple[str, str]:
        return self.simplifier.correct(key)
