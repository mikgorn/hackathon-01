from typing import *

import base


class Answer(base.Answer):
    def __init__(self, eps: int, value: Any):
        self.eps = eps
        self.value = value

    def __repr__(self):
        return "Answer(eps: %d, values: %s)" % (self.eps, str(self.value))


class Branch(base.Branch):
    def __init__(self, id: int):
        self.id = id
        #self.parents = []
        self.children = []
        self.values = []

    def __repr__(self):
        return "Branch(%d)%s" % (self.id, '\n'.join(['\t'.join([str(i) for i in self.children]) if self.children else '']))

    def __hash__(self):
        return self.id


class Tree(base.Tree):
    def __init__(self, words: Dict[str, int], words_counter: Dict[int, int]): #simplifier: base.Simplifier):
        #self._simplifier = simplifier
        self._words = words
        self._words_counter = words_counter
        self._branches: List[base.Branch] = []

    def _get_indexes(self, keys: List[str]) -> List[int]:
        if not keys:
            return []
        keys = list(set(keys))
        tmp_res = sorted([self._words[i] for i in keys], key=lambda w: self._words_counter[w], reverse=True)
        sl = []
        result = []
        last_value = self._words_counter[tmp_res[0]]
        for i in tmp_res:
            if i in sl:
                continue
            if self._words_counter[i] == last_value:
                sl.append(i)
                continue
            else:
                sl.append(i)
                result.extend(sorted(sl))
                sl = []
                last_value = self._words_counter[i]
        else:
            result.extend(sorted(sl))
        return result

    def _find_in_branch(self, branch: base.Branch, id: int) -> Optional[base.Branch]:
        if branch.id == id:
            #print(branch.id)
            return branch
        if self._words_counter[branch.id] < self._words_counter[id]:
            return
        for children in branch.children:
            return self._find_in_branch(children, id)

    def _get_branch(self, index: int) -> Optional[base.Branch]:
        for branch in self._branches:
            result = self._find_in_branch(branch, index)
            if result is not None:
                return result
        else:
            return

    def set_value(self, key: List[str], value: Any) -> None:
        indexes = self._get_indexes(key)
        if not indexes:
            raise Exception("Unknown words in '%s' \n Words: %s \n Words counter: %s" % (key, str(self._words), str(self._words_counter)))
        start_index = indexes.pop(0)
        last_branch: Optional[base.Branch] = self._get_branch(start_index)
        was_branch_created = False
        if last_branch is None:
            last_branch = Branch(start_index)
            self._branches.append(last_branch)
        branch = None
        for index in indexes:
            branch = self._get_branch(index)
            if branch is None:
                branch = Branch(index)
                last_branch.children.append(branch)
                was_branch_created = True
            elif was_branch_created:
                last_branch.children.append(branch)
                was_branch_created = False
            else:
                if branch not in last_branch.children:
                    last_branch.children.append(branch)
            last_branch = branch
        if branch is None:
            last_branch.values.append(value)
            return
        branch.values.append(value)

    def get_values(self, key: List[str]) -> List[Answer]:
        result = []
        indexes = self._get_indexes(key)
        if not indexes:
            return []
        eps = 1
        first_branch = self._get_branch(indexes.pop(0))
        if first_branch is None:
            return result
        return self._get_values(first_branch, indexes, set(), eps)

    def _get_all_values(self, branch: base.Branch, checked: Set[base.Branch], eps: int) -> List[Answer]:
        result = []
        checked.add(branch)
        for child in branch.children:
            if child in checked:
                continue
            checked.add(child)
            if child.values:
                result.append(Answer(eps, child.values))
            result.extend(self._get_all_values(child, checked, eps))
        else:
            return result

    def _get_values(self, branch: base.Branch, indexes: List[int], checked: Set[base.Branch], eps: int) -> List[Answer]:
        if not branch.children:
            return [Answer(eps + 1, branch.values)]
        if branch in checked:
            return []
        if not indexes:
            result = []
            result.extend(self._get_all_values(branch, checked, eps))
            return result
        next_branch = self._get_branch(indexes.pop(0))
        if next_branch is None:
            return self._get_all_values(branch, checked, eps)
        if next_branch in branch.children:
            checked.add(branch)
            return self._get_values(next_branch, indexes, checked, eps + 1)
        result = []
        path = [i for i in reversed(self._find_branch_path(branch, next_branch.id))]
        last_branch = path[-1]
        result.extend(self._pather(path, checked, eps))
        result.extend(self._get_values(last_branch, indexes, checked, eps))
        return result

    def _find_branch_path(self, branch: base.Branch, need_id: int,) -> List[base.Branch]:
        if self._words_counter[branch.id] < self._words_counter[need_id]:
            return []
        for child in branch.children:
            if child.id == need_id:
                return [child, branch]
        result = []
        for child in branch.children:
            result = self._find_branch_path(child, need_id)
            if result:
                result.append(branch)
                return result
        return result

    def _pather(self, path: List[base.Branch], checked: Set[Branch], eps: int) -> List[Answer]:
        last_branch = path.pop(-1)
        result = []
        for branch in path:
            checked.add(branch)
            if branch.values:
                result.append(Answer(eps, branch.values))
        result.append(Answer(eps + 1, last_branch.values))
        return result

    def delete_value(self, key: List[str], value: Any) -> None:
        branch = self._get_branch(self._get_indexes(key)[-1])
        if branch is None:
            return
        branch.values.pop(branch.values.index(value))

    def delete_branch(self, key: List[str]) -> None:
        raise NotImplementedError
