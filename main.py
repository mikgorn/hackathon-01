from db_processor import *
from crutch_tree import Tree
import time

def main():
        
        words,words_counter,products = create_db()
        start = time.time()
        tree = Tree(words, words_counter)
        time1 = time.time()
        for key,value in products.items():
                fullname = key.split()
                print(key+" "+str(value))
                tree.set_value(fullname,int(value))
        time2 = time.time()

        print("_________________")
        print("Tree creation time "+str(time1-start))
        print("Set value time "+str(time2-time1))

        res = tree.get_values(["напольный", "вентилятор"])
        print(res)

if __name__ == "__main__":
    main()