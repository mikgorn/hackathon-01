from typing import *
from abc import ABCMeta, abstractmethod


class Answer(metaclass=ABCMeta):
    eps: int
    value: Any

    @abstractmethod
    def __init__(self, eps: int, value: Any):
        ...


class Simplifier(metaclass=ABCMeta):
    @abstractmethod
    def equal_words(self, word: str) -> List[str]:
        ...

    @abstractmethod
    def simplify(self, text: str) -> str:
        ...

    @abstractmethod
    def correct(self, text: str) -> Tuple[str, str]:
        ...

    @abstractmethod
    def normalize(self, word: str) -> str:
        ...


class Branch(metaclass=ABCMeta):
    # parents: List["Branch"]
    children: List["Branch"]
    values: List[Any]
    id: int

    @abstractmethod
    def __init__(self, id: int):
        ...


class Tree(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self, words: Dict[str, int], words_counter: Dict[str, int], simplifier: Simplifier):
        ...

    @abstractmethod
    def set_value(self, key: str, value: Any) -> None:
        ...

    @abstractmethod
    def get_values(self, key: str) -> List[Answer]: ...

    @abstractmethod
    def delete_value(self, key: str, value: Any) -> None:
        ...

    @abstractmethod
    def delete_branch(self, key: str) -> None:
        ...


class Database(metaclass=ABCMeta):
    length: int

    @abstractmethod
    def set_value(self, id: Any, value: Any) -> None:
        ...

    @abstractmethod
    def delete_value(self, id: Any) -> None:
        ...

    @abstractmethod
    def get_values_using_filter(self, predicate: Callable) -> List[Any]:
        ...

    @abstractmethod
    def get_values(self, id: Any) -> List[Any]:
        ...

    @abstractmethod
    def get_value(self, id: Any, col: Any):
        ...

    @abstractmethod
    def get_vertical_vector(self, start: int, stop: int, col:int) -> List[Any]:
        ...

    @abstractmethod
    def __getitem__(self, item):
        ...


class Simplifier(metaclass=ABCMeta):
    @abstractmethod
    def simplify(self, text: str) -> List[str]:
        ...

    @abstractmethod
    def correct(self, text: str) -> Tuple[str, str]:
        ...

    @abstractmethod
    def get_equals(self, word: str) -> List[str]:
        ...


class Finder(metaclass=ABCMeta):
    words: Optional[Dict[str, int]]
    words_counter: Optional[Dict[int, int]]

    @abstractmethod
    def __init__(self, db: Database, simplifier: Simplifier):
        ...

    @abstractmethod
    def get_values(self, key: List[str]) -> List[Any]:
        ...


class Core(metaclass=ABCMeta):
    db: Database
    simplifier: Simplifier

    @abstractmethod
    def get_answer(self, key: str) -> List[Any]:
        ...

    @abstractmethod
    def check_errors(self, key: str) -> Tuple[str, str]:
        ...
