import sqlite3
import xlrd
import time
import pymorphy2
from category import *
from collections import defaultdict
def process_line(line):
        line = line.lower()
        line = line.translate({ord(c): " " for c in "!@#$%^&*()[]\{\}\"\';:,./<>?\|`~-=_+«»"})
        return line


def create_db():
        morph = pymorphy2.MorphAnalyzer()
        all_words =defaultdict(int)

        categories = [init_root()]
        time_start = time.time()
        conn = sqlite3.connect('Hackathon_01.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE If NOT EXISTS products (id int, name text, manufacturer text, country text, type1 text ,type0 text , length text , width text , height text, material text , diameter text ,warranty text , color text , weight text , volume text , offer_amount int , contract_sum text , contract_amount int  ) ''')
        excel_file = xlrd.open_workbook('Dataset.xlsx')
        excel_sheet = excel_file.sheet_by_index(0)
        first_row = []
        for col in range(excel_sheet.ncols):
                first_row.append(excel_sheet.cell_value(0,col))
        #print("INSERT INTO products VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ",first_row)
        #c.execute("INSERT INTO products VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ",first_row)
        for nrow in range (1,100):#excel_sheet.nrows):
                row = excel_sheet.row_values(nrow)
                #c.execute("INSERT INTO products VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ",row)
                fullname=[]
                name = ""
                try:
                        name = process_line(row[1])
                        fullname = name.split()
                except:
                        print("error")
                cat1 = row[4]
                cat0 = row[5]
                cat = find_category(categories,cat0)
                if cat==None:
                        cat = Category()
                        cat.name = cat0
                        cat.parent_name = cat1
                        categories.append(cat)
                        print("Добавлена категория \""+cat1+"\" в  \""+cat0+'\"')
                for word in fullname:
                        if not (word in all_words):
                                p = morph.parse(word)[0]
                                p_norm=p.normal_form
                                all_words[p_norm]+=1
                                cat.words[p_norm]+=1
                                cat.products.update({name:row[0]})

        #conn.commit()
        now = time.time()

        #Update categories, make a Tree
        #for i in range(5):

        #        for category in categories:
        #                if((category.parent==None)&(category.parent_name!="")):
        #                        parent = find_category(categories,category.parent_name)
        #                        print(category.parent_name)
        #                        add_child(parent,category)

        #print_category(categories[0])
        print("Elapsed time ",now-time_start)
        

        i=0
        words={}
        words_counter={}
        for pair in all_words.items():
                words.update({pair[0]:i})
                words_counter.update({i:pair[1]})
                i+=1
        return words,words_counter,categories[1].products
        
