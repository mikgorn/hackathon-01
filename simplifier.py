import re
import logging
import requests
from typing import *

import pymorphy2

import base


class YandexCorrector:
    _url = "https://speller.yandex.net/services/spellservice.json/checkText?text=%s"

    def correct(self, word: str) -> Optional[str]:
        result = requests.post(self._url % word.replace(' ', '+'), headers={"Content-Type": "application/json"}, timeout=0.5).json()
        if not result:
            return None
        return result[0]['s'][0]


class Simplifier(base.Simplifier):

    SPECIAL_SYMBOLS = re.compile(r"[^\w\s]]+")
    SPACE = re.compile(r"\s+")

    def __init__(self):
        self._yandex_corrector = YandexCorrector()
        self._inner_corrector = None
        self._normalizer = pymorphy2.MorphAnalyzer()

    unnecessary_words = {'на', 'с', 'в', 'для'}

    def get_equals(self, word: str) -> List[str]:
        return []

    def simplify(self, text: str) -> List[str]:
        text = text.lower()
        #text = re.sub(self.SPECIAL_SYMBOLS, text, '')
        #text = set(re.split(self.SPACE, text))
        text = text.translate({ord(c): " " for c in "!@#$%^&*()[]\{\}\"\';:,./<>?\|`~-=_+«»"})
        text = re.sub(self.SPACE, " ", text)
        text = set(text.strip().split(' '))
        # TODO: Is one space:?
        result = []
        for word in text:
            if word in self.unnecessary_words:
                continue
            result.append(word)
        #return [self.normalize(word) for word in result]
        return result

    def correct(self, text: str) -> Tuple[str, str]:
        try:
            result = text.split(' ')
            for i in range(len(result)):
                tmp_result = self._yandex_corrector.correct(result[i])
                if tmp_result:
                    result[i] = tmp_result
            result_text = ' '.join(result)
            if result_text == text:
                return text, ""
            return text, result_text
        except Exception as e:
            logging.error(e)
            return text, ""

    def normalize(self, word: str) -> str:
        return self._normalizer.parse(word)[0]