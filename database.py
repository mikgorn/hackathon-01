from typing import *

import xlrd

import base


def connect_to_db(path: Any) -> base.Database:
    # If xlrd
    return DatabaseXlrd(path)


class DatabaseXlrd(base.Database):

    def __init__(self, path_to_file: str, sheet_index: int = 0):
        self.data = xlrd.open_workbook(path_to_file).sheet_by_index(sheet_index)
        self._values_count = self.data.ncols
        self.length = self.data.nrows

    def get_values(self, key: int) -> List[Any]:
        return self.data.row_values(key)

    def get_values_using_filter(self, predicate: Callable) -> List[Any]:
        result = []
        for i in range(self.data.nrows):
            values = self.get_values(i)
            if predicate(values):
                result.append(values)
        return result

    def get_value(self, key: int, col: int) -> Any:
        return self.data.cell_value(key, col)

    def get_vertical_vector(self, start: int, stop: int, col: int) -> List[Any]:
        return self.data.col_values(col, start, stop)

    def __getitem__(self, item):
        if isinstance(item, slice):
            return [self.get_values(key) for key in range(item.start, item.stop, item.step if item.step else 1)]
        else:
            return self.get_values(item)

    def delete_value(self, key: int) -> None:
        ...

    def set_value(self, key: int, value: Any) -> None:
        ...
